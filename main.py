import time
import threading
from lib.elements import RPIObject
from lib.conf import ConfigurationObject
from lib.gps import GPSObject
from lib.mqtt import MqttObject
from lib.storage import storage_message
from lib.distancesensor import DistanceSensor


configuration = ConfigurationObject()
ID_SINAPSIS = configuration.config_dict['sinapsis']['id']

time_laps = 3

def build_json_data(data_to_build_json):
    json_data = {
            'sinapsis':{
                'id': ID_SINAPSIS,
                'mode': data_to_build_json['mode']
            },
            'devices':{
                'gps':{
                    'latitude': str(data_to_build_json['gps_data']['latitude']),
                    'longitude': str(data_to_build_json['gps_data']['longitude']),
                    'speed': str(data_to_build_json['gps_data']['speed']),
                    'altitude': str(data_to_build_json['gps_data']['altitude']),
                    'motion_direction': str(data_to_build_json['gps_data']['motion_dir']),
                    'climb': str(data_to_build_json['gps_data']['climb']) 
                },
                'buttons':{
                    'panico': 0 #por definir
                },
                'distance':{
                    'distance_level_1': str(data_to_build_json['level']['1']),
                    'distance_level_2': str(data_to_build_json['level']['2'])
                }
            },
            'info_travel': data_to_build_json['info_travel'],
            'message_details': {
                'date': str(data_to_build_json['gps_data']['date_time']),
                'id_message':str(0)
                }
        }
    return json_data




def main():
    core = RPIObject()
    core.internet_status_thread() #internet thread
    core.switch_signal_thread() #Signal Switch to shutdown

    keep_alive = core.execute('keep_alive')

    threading.Thread(target=core.life_signal).start()

    gps_client = GPSObject()
    storage_first_position_thread = threading.Thread(target=gps_client.store_first_position)
    storage_first_position_thread.start()


    mqtt_client = MqttObject()
    mqtt_client.run()

    distance_sensor = DistanceSensor()


    while True:
        while mqtt_client.connected == True:
            gps_data = gps_client.get_data()
            
            if gps_data:
                inside_perimeter = gps_client.perimeter(
                    current_coord=(gps_data['latitude'],gps_data['longitude'])
                    )

                if inside_perimeter:
                    data_to_build_json = {}              

                    info_travel = gps_client.info_travel(gps_data) #i need take hour from gps because i don't have RTC

                    data_to_build_json['info_travel'] = info_travel
                    data_to_build_json['mode'] = core.report_mode()
                    data_to_build_json['gps_data'] = gps_data

                    data_to_build_json['level'] = distance_sensor.how_many_distance()

                    data = build_json_data(data_to_build_json)
                    
                    try:
                        mqtt_client.publish_message(msg=data)
                    except Exception as e:
                        pass

                    gps_client.set_last_coord(
                        last_coord=(gps_data['latitude'],gps_data['longitude']))

                    gps_data=False
            else:
                print('GPS not available')

            time.sleep(time_laps) #Tiempo de cada vuelta para el envio de coordenadas, ajustar a variable


        while not mqtt_client.connected:
            mqtt_client.run()
            gps_data = gps_client.get_data()

            if gps_data:
                inside_perimeter = gps_client.perimeter(
                    (gps_data['latitude'],gps_data['longitude'])
                    )

                if inside_perimeter:


                    data_to_build_json = {}

                    info_travel = gps_client.info_travel(gps_data) #i need take hour from gps because i don't have RTC

                    data_to_build_json['info_travel'] = info_travel
                    data_to_build_json['mode'] = core.report_mode()
                    data_to_build_json['gps_data'] = gps_data
                    data_to_build_json['level'] = distance_sensor.how_many_distance()

                    data = build_json_data(data_to_build_json)
                    
                    storage_message(msg=data, now=gps_data['date_time'])

                    gps_client.set_last_coord(
                        last_coord=(gps_data['latitude'],gps_data['longitude']))

                    gps_data=False
                    
            time.sleep(time_laps)



if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Sinapsis Main Script stopped\n')
        print('Keep_Alive Main Script stopped\n')
