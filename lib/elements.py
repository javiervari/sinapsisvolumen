import os, time, subprocess, sys
import ast
import threading
import RPi.GPIO as GPIO

from .conf import ConfigurationObject



class RPIObject(ConfigurationObject):
    def __init__(self):
        ConfigurationObject.__init__(self)

        self.gpio_life_signal = ast.literal_eval(self.config_dict['MCU']['gpio_life_signal'])
        self.gpio_router = ast.literal_eval(self.config_dict['MCU']['gpio_router'])
        self.gpio_rpi_mode = ast.literal_eval(self.config_dict['MCU']['gpio_rpi_mode'])
        self.gpio_onoff = ast.literal_eval(self.config_dict['MCU']['gpio_onoff'])
        
        self.gpio_trigger1 = ast.literal_eval(self.config_dict['MCU']['gpio_onoff'])
        self.gpio_echo1 = ast.literal_eval(self.config_dict['MCU']['gpio_onoff'])

        self.gpio_trigger2 = ast.literal_eval(self.config_dict['MCU']['gpio_onoff'])
        self.gpio_echo2 = ast.literal_eval(self.config_dict['MCU']['gpio_onoff'])

        self.internet = 0 #limit 10
        
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        
        GPIO.setup(self.gpio_life_signal, GPIO.OUT)
        GPIO.setup(self.gpio_onoff, GPIO.OUT)
        GPIO.setup(self.gpio_router, GPIO.OUT)
        GPIO.setup(self.gpio_rpi_mode, GPIO.IN)
        
        self.base_path = os.path.abspath('/home/pi/sinapsisvolumen/')


    def internet_status_thread(self): #cambiar por clase de modulo gsm(class GSMModule)
        threading.Timer(60, self.internet_status_thread).start()
        hostname = 'google.com'
        response = os.system("ping -c 1 {} >/dev/null 2>&1".format(hostname))
        if response == 0:
            self.internet = 0 #limit 5
        else:
            self.internet +=1
            if self.internet > 5:
                GPIO.output(self.gpio_router, GPIO.HIGH)
                time.sleep(5)
                GPIO.output(self.gpio_router, GPIO.LOW)
                self.internet = 0 #limit 5



    def switch_signal_thread(self):
        threading.Timer(10, self.switch_signal_thread).start()
        if GPIO.input(self.gpio_onoff) != 0:
            GPIO.cleanup () #clean GPIOs
            os.system("sudo shutdown now")








    def report_mode(self):
        mode = 'sleep' if GPIO.input(self.gpio_rpi_mode) == 0 else 'normal' #0: Sleep - 1: Normal
        return mode



    def execute(self, file):
        if file == 'keep_alive':
            file = self.base_path+'/keep_alive.py'
        else:
            return False
        
        cmd_string = 'python3 {}'.format(file)
        process = subprocess.Popen(cmd_string.split(), stdout=sys.stdout, stderr=sys.stderr)
        return process



    def life_signal(self):
        while True:
            GPIO.output(self.gpio_life_signal, GPIO.HIGH)
            time.sleep(3)
            GPIO.output(self.gpio_life_signal, GPIO.LOW)
            time.sleep(3)