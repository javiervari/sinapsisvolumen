import os, time, json
import psutil as ps

import paho.mqtt.client as mqttc
from cryptography.fernet import Fernet

from .conf import ConfigurationObject
from .gps import GPSObject




class MqttObject(ConfigurationObject, GPSObject):
    """[summary]
    
    Arguments:
        ConfigurationObject {[type]} -- [description]
    """

    def __init__(self):
        ConfigurationObject.__init__(self)
        GPSObject.__init__(self)
        self.broker = self.config_dict['mqtt']['broker']
        self.connected = False
        self.topic_sinapsis = self.config_dict['mqtt']['topic_sinapsis']
        self.topic_message = self.config_dict['mqtt']['topic_message']
        self.topic_status = self.config_dict['mqtt']['topic_status']
        self.topic_alert = self.config_dict['mqtt']['topic_alert']
        self.topic_error = self.config_dict['mqtt']['topic_error']

        self.key = "UOC-DMXptS_t7JPi_omDofSfTYaYVTqn638NLMl_zYE="
        self.cipher_suite = Fernet(self.key)

        self.client = mqttc.Client()
        self.connected = False
        

    def on_message(self, client, userdata, message):
        message = message.payload.decode('utf-8')
        if message=='info':
            try:
                    
                info_dict={'sinapsis_id':self.config_dict['sinapsis']['id']}
                
                gps_data = self.get_data()
                info_dict['coordenadas'] = "{}, {}".format(gps_data['latitude'], gps_data['longitude'])

                info_dict['cpu'] = ps.cpu_percent()

                response = os.system("ping -c 1 {} 2>&1".format('google.com'))
                internet = True if response==0 else False
                info_dict['internet'] = internet
                self.publish_message(msg=info_dict)
                

            except Exception as e :
                print('MQTT on_message error',e)


        elif message=='coord':
            gps_data = self.get_data()
            coordinates = "{},{}".format(gps_data['latitude'], gps_data['longitude'])
            if coordinates == 'None,None':
                coordinates=False

            coordinates = {'coordinates':coordinates}
            self.publish_message(msg=coordinates)



    def on_disconnect(self, client, userdata, rc):
        print('MQTT Disconnected')
        self.connected = False



    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.connected = True
            self.client.subscribe(self.topic_sinapsis, 1)
            print("Connected to topic {}".format(self.topic_sinapsis))
        else:
            print('Trying to connect to MQTT')
            self.connected = False


    def run(self):
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_message = self.on_message
        try:
            self.client.connect(self.broker,1883,60)
            self.client.loop_start()
            time.sleep(2)
        except Exception as e:
            print('MQTT Error',e)
            self.connected = False



    def publish_message(self, msg, topic=None):
        if topic == None: topic=self.topic_message
        msg = json.dumps(msg)
        msg = self.cipher_suite.encrypt(bytes(msg,encoding='utf8'))
        status = self.client.publish(topic, msg, 1)
        # return status[0]


