import os, time

from configparser import ConfigParser

class ConfigurationObject(): 
    def __init__(self):
        self.conf_path = '/home/pi/sinapsisvolumen/conf.cfg'
        self.config=ConfigParser() 
        self.config.read(self.conf_path) 
        if self.config['sinapsis']['id'] == '': 
            self.serialization()
            self.config.set('sinapsis', 'id', self.sinapsis_id)
            with open(self.conf_path, 'w') as configfile:
                self.config.write(configfile)

        self.values()


    def serialization(self):
        router_mac = ['']
        while router_mac == ['']:
            cmd_to_get_mac = os.popen("arp -a | grep '(192.168.1.1)' | awk '{ print $4 }'")
            output = cmd_to_get_mac.read()
            router_mac = output[:-1].split(':')
            time.sleep(1)

        # MAC = open('/sys/class/net/eth0/address').read()[0:17].replace(':', '').upper()[-6::]
        self.sinapsis_id = 'VKA-B-A011-CA{}-{}{}'.format(router_mac[-3],router_mac[-2],router_mac[-1]).upper()


    def values(self):
        sections = self.config.sections()
        self.config_dict = {}

        for section in sections:
            self.config_dict[section] = {}

            for item in self.config.items(section):
                self.config_dict[section][item[0]] = item[1]



