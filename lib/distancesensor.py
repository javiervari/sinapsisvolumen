import statistics
import time
import ast
import RPi.GPIO as GPIO
from interruptingcow import timeout

from .conf import ConfigurationObject




class DistanceSensor(ConfigurationObject):
    def __init__(self):
        ConfigurationObject.__init__(self)

        self.trigger1 = ast.literal_eval(self.config_dict['MCU']['gpio_trigger1'])
        self.echo1 = ast.literal_eval(self.config_dict['MCU']['gpio_echo1'])

        self.trigger2 = ast.literal_eval(self.config_dict['MCU']['gpio_trigger2'])
        self.echo2 = ast.literal_eval(self.config_dict['MCU']['gpio_echo2'])

        self.truck_size = 4 #meters
        
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)

        GPIO.setup(self.trigger1, GPIO.OUT)
        GPIO.setup(self.trigger2, GPIO.OUT)

        GPIO.setup(self.echo1, GPIO.IN)
        GPIO.setup(self.echo2, GPIO.IN)




    @timeout(2)
    def measure(self, sensor):
        measures = []

        if sensor==1:
            trigger = self.trigger1
            echo = self.echo1

        elif sensor==2:
            trigger = self.trigger2
            echo = self.echo2


        for i in range(3):
            GPIO.output(trigger, False)  # Trigg OFF
            time.sleep(10*10**-6)  # wait 2us

            GPIO.output(trigger, True)  # Trigg ON
            time.sleep(10*10**-6)  # wait 2us

            GPIO.output(trigger, False)  # Trigg OFF

            StartTime = time.time()
            StopTime = time.time()

            while GPIO.input(echo) == 0:
                StartTime = time.time()

            while GPIO.input(echo) == 1:
                StopTime = time.time()

            TimeElapsed = StopTime - StartTime  # Time difference btwn start and arrival
            
            measure = ((TimeElapsed * 34300) / 2) / 100.0 # Div 100 to convert from cm to meters

            if (0 <= measure <= self.truck_size):
                measures.append(measure)


        try:
            avg_distance_measures = statistics.mean(measures)
            filling_percentage = 100.0 - (round((avg_distance_measures*100)/self.truck_size,1))
            filling_percentage = round(filling_percentage,2)
            return filling_percentage #in %

        except Exception as e:
            print(e)
            return 'None'





    def how_many_distance(self):
        try:
            level1 = self.measure(1)
        except Exception as e:
        # except RuntimeError:
            level1 = 'None'


        try:
            level2 = self.measure(2)
        except Exception as e:
        # except RuntimeError:
            level2 = 'None'

        return {'1': level1, '2':level2}